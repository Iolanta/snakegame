// Copyright Epic Games, Inc. All Rights Reserved.

#include "SnakeGameTest.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SnakeGameTest, "SnakeGameTest" );
