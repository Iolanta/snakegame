// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGameTestGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAMETEST_API ASnakeGameTestGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
